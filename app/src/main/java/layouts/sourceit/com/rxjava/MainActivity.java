package layouts.sourceit.com.rxjava;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;
import rx.functions.Func1;

public class MainActivity extends AppCompatActivity {

    Subscription subscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        subscription = Observable.just("Hello World", 1)
                .map(new Func1<Object, Integer>() {
                    @Override
                    public Integer call(Object obj) {

                       return obj.toString().split("l").length-1;
                      // return obj.toString().length() - obj.toString().replace("l", "").length();
                    }

                })
                .subscribe(new Action1<Integer>() {
                    @Override
                    public void call(Integer s) {
                        Toast.makeText(MainActivity.this,s.toString(), Toast.LENGTH_LONG).show();
                    }
                });


    }
    @Override
    protected void onPause() {
        super.onPause();
        if (subscription != null && !subscription.isUnsubscribed()){
            subscription.unsubscribe();
        }
    }

}
